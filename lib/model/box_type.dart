enum BoxType {
  text,
  image;

  static BoxType fromJson(String json) => values.byName(json);
}
