import 'dart:convert';

import 'package:boxes_game/gen/assets.gen.dart';
import 'package:flutter/services.dart';

class AssetsHelper {
  Future<dynamic> loadGameJsonFromAssets() async {
    return _loadJsonFromAssets(Assets.game);
  }

  Future<dynamic> _loadJsonFromAssets(String path) async {
    final jsonString = await rootBundle.loadString(path);
    return jsonDecode(jsonString);
  }
}
