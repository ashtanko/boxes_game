import 'package:bloc/bloc.dart';
import 'package:boxes_game/assets_helper.dart';
import 'package:boxes_game/model/model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'game_event.dart';

part 'game_state.dart';

class GameBloc extends Bloc<GameEvent, GameState> {
  var _allLeftBoxes = <Box>[];
  var _allRightBoxes = <Box>[];

  final AssetsHelper assetsHelper;

  GameBloc(this.assetsHelper) : super(GameInitial()) {
    on<LoadGameDataEvent>((event, emit) async {
      emit(GameLoading());
      try {
        final jsonData = await assetsHelper.loadGameJsonFromAssets();
        final boxes = Boxes.fromJson(jsonData);
        _allLeftBoxes = boxes.left;
        _allRightBoxes = boxes.right;
        emit(GameLoaded(left: _allLeftBoxes, right: _allRightBoxes));
      } catch (e) {
        emit(GameLoadError());
      }
    });

    on<RemoveBoxEvent>((event, emit) async {
      final currentId = event.current.id;
      final targetId = event.target.id;

      final targetBox = _allLeftBoxes.firstWhere((box) => box.id == targetId);
      final targetBoxIndex = _allLeftBoxes.indexOf(targetBox);
      final selectedBox =
          _allRightBoxes.firstWhere((box) => box.id == currentId);
      final boxToAdd = targetBox.copyWith(
        boxSet: {selectedBox},
        hasError: event.current.id != event.target.pairId,
      );
      _allLeftBoxes[targetBoxIndex] = boxToAdd;

      // update the game state based on changed boxes
      emit(
        GameLoaded(
          left: List.of(_allLeftBoxes),
          right: List.of(_allRightBoxes)..remove(selectedBox),
        ),
      );
      _allRightBoxes.remove(selectedBox);

      if (_allRightBoxes.isEmpty) {
        var errors = _allLeftBoxes
            .map((e) => e.hasError)
            .where((element) => element == true)
            .toList();
        emit(GameFinished(errors.length));
      }
    });
  }
}
