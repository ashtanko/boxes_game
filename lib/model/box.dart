import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'model.dart';

@immutable
class Box extends Equatable {
  final String id;
  final String pairId;
  final BoxPosition position;
  final BoxType type;
  final String? content;
  final String? url;
  final Set<Box> boxSet;
  final bool hasError;

  const Box({
    required this.id,
    required this.pairId,
    required this.position,
    required this.type,
    this.content,
    this.url,
    this.boxSet = const {},
    this.hasError = false,
  });

  Box copyWith({
    String? id,
    String? pairId,
    BoxPosition? position,
    BoxType? type,
    String? content,
    String? url,
    Set<Box> boxSet = const {},
    bool hasError = false,
  }) {
    return Box(
      id: id ?? this.id,
      pairId: pairId ?? this.pairId,
      position: position ?? this.position,
      type: type ?? this.type,
      content: content ?? this.content,
      url: url ?? this.url,
      boxSet: boxSet,
      hasError: hasError,
    );
  }

  factory Box.fromJson(dynamic json, BoxPosition position) {
    return Box(
      id: json["id"] as String,
      pairId: json["pair_id"],
      content: json["content"],
      url: json["url"],
      position: position,
      type: BoxType.fromJson(json["type"]),
    );
  }

  @override
  List<Object?> get props => [
        id,
        pairId,
        position,
        type,
        content,
        url,
        boxSet,
        hasError,
      ];
}
