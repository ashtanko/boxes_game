import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import 'model.dart';

@immutable
class Boxes extends Equatable {
  final List<Box> left;
  final List<Box> right;

  const Boxes({required this.left, required this.right});

  factory Boxes.fromJson(dynamic json) {
    final left = json["left"] as List<dynamic>;
    final leftBoxes =
        left.map((e) => Box.fromJson(e, BoxPosition.left)).toList();
    final right = json["right"] as List<dynamic>;
    final rightBoxes =
        right.map((e) => Box.fromJson(e, BoxPosition.right)).toList();
    return Boxes(
      left: leftBoxes,
      right: rightBoxes,
    );
  }

  @override
  List<Object?> get props => [left, right];
}
