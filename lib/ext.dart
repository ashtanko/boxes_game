import 'package:boxes_game/model/model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

extension ScreenSize on BuildContext {
  double gameGridWidth() {
    return MediaQuery.of(this).size.width / 2;
  }

  double gameGridHeight() {
    return MediaQuery.of(this).size.height;
  }
}

extension BoxContent on BuildContext {
  Widget getBoxContent(Box box) {
    if (box.type == BoxType.image &&
        box.url != null &&
        box.url?.isNotEmpty == true) {
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Image.network(box.url!),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(box.content ?? loc.no_data_title,
            style: Theme.of(this).textTheme.titleMedium),
      );
    }
  }
}

extension LocalizedBuildContext on BuildContext {
  AppLocalizations get loc => AppLocalizations.of(this);
}
