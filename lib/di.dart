import 'package:boxes_game/assets_helper.dart';
import 'package:boxes_game/bloc/game_bloc.dart';

final assetsHelper = AssetsHelper();
final gameBlockInstance = GameBloc(assetsHelper);
