part of 'game_bloc.dart';

@immutable
abstract class GameEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoadGameDataEvent extends GameEvent {}

class RemoveBoxEvent extends GameEvent {
  final Box current;
  final Box target;

  RemoveBoxEvent(this.current, this.target);

  @override
  List<Object?> get props => [current, target];
}
