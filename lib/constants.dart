const double boxSize = 90; // normal box size
const double stackedBoxSize = 75; // the box size when it stacked
const double gameScreenPadding = 8.0;
const double rightGridOffset = 16.0;
const double resetGameIconSize = 26.0;
