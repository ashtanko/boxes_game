import 'package:boxes_game/bloc/game_bloc.dart';
import 'package:boxes_game/constants.dart';
import 'package:boxes_game/di.dart';
import 'package:boxes_game/ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'model/model.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight])
      .then((_) {
    runApp(const App());
  });
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => gameBlockInstance..add(LoadGameDataEvent()),
      child: MaterialApp(
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        home: Scaffold(
          appBar: AppBar(
            title: const Text("Flutter Drag and Drop"),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    gameBlockInstance.add(LoadGameDataEvent());
                  },
                  child: const Icon(
                    Icons.refresh,
                    size: resetGameIconSize,
                  ),
                ),
              ),
            ],
          ),
          body: BlocListener<GameBloc, GameState>(
            listener: (context, state) {
              if (state is GameFinished) {
                showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: Text(context.loc.finish_title),
                    content:
                        Text(context.loc.finish_message_title(state.errors)),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () {
                          gameBlockInstance.add(LoadGameDataEvent());
                          Navigator.pop(
                              context, context.loc.start_new_game_title);
                        },
                        child: Text(context.loc.start_new_game_title),
                      ),
                      TextButton(
                        onPressed: () =>
                            Navigator.pop(context, context.loc.ok_title),
                        child: Text(context.loc.ok_title),
                      ),
                    ],
                  ),
                );
              }
            },
            child: BlocBuilder<GameBloc, GameState>(
              buildWhen: (previous, current) => current is GameLoaded,
              builder: (context, state) {
                if (state is GameLoading) {
                  return const Center(child: CircularProgressIndicator());
                }

                if (state is GameLoaded) {
                  return Padding(
                    padding: const EdgeInsets.all(gameScreenPadding),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      verticalDirection: VerticalDirection.down,
                      children: <Widget>[
                        SizedBox(
                          width: context.gameGridWidth(),
                          height: context.gameGridHeight(),
                          child: _buildSection(
                              context, state.left, WrapAlignment.start),
                        ),
                        SizedBox(
                          width: context.gameGridWidth() - rightGridOffset,
                          height: context.gameGridHeight(),
                          child: _buildSection(
                              context, state.right, WrapAlignment.end),
                        ),
                      ],
                    ),
                  );
                }

                if (state is GameLoadError) {
                  return Center(child: Text(context.loc.error_title));
                }
                return Center(child: Text(context.loc.no_data_title));
              },
            ),
          ),
        ),
      ),
    );
  }

  static Widget _buildSection(
      BuildContext context, List<Box> boxes, WrapAlignment alignment) {
    if (boxes.isEmpty) return const SizedBox();
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      primary: false,
      child: Wrap(
        alignment: alignment,
        direction: Axis.horizontal,
        runAlignment: WrapAlignment.center,
        crossAxisAlignment: WrapCrossAlignment.center,
        verticalDirection: VerticalDirection.down,
        spacing: 10.0,
        runSpacing: 10.0,
        children: boxes.map((box) {
          return BoxWidget(
            box: box,
          );
        }).toList(),
      ),
    );
  }
}

class ErrorBorderWidget extends StatelessWidget {
  final bool hasError;
  final Widget child;
  final double borderWidth;
  final Color borderColor;
  final Color errorBorderColor;

  const ErrorBorderWidget({
    Key? key,
    required this.hasError,
    required this.child,
    this.borderWidth = 2.0,
    this.borderColor = Colors.green,
    this.errorBorderColor = Colors.red,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: borderWidth,
          color: hasError ? errorBorderColor : borderColor,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: child,
      ),
    );
  }
}

class StackedBoxesWidget extends StatelessWidget {
  final List<Box> boxes;

  const StackedBoxesWidget({Key? key, required this.boxes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final boxWidgets = <Widget>[];
    var offset = 0.0;
    for (var i = 0; i <= boxes.length - 1; i++) {
      final box = boxes[i];
      boxWidgets.add(Positioned(
        left: offset,
        top: offset,
        child: Container(
          width: stackedBoxSize,
          height: stackedBoxSize,
          decoration: BoxDecoration(
              color: Colors.greenAccent,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 10,
                  offset: const Offset(0, 3),
                )
              ]),
          child: Center(child: context.getBoxContent(box)),
        ),
      ));
      offset += 15;
    }

    return SizedBox(
      width: boxSize,
      height: boxSize,
      child: Stack(
        children: [...boxWidgets],
      ),
    );
  }
}

class BoxWidget extends StatelessWidget {
  final Box box;

  const BoxWidget({super.key, required this.box});

  @override
  Widget build(BuildContext context) {
    return DragTarget<Box>(
      onWillAccept: (value) => value?.position != box.position,
      builder: (context, accepted, rejected) {
        return box.position == BoxPosition.right
            ? Draggable<Box>(
                data: box,
                feedback: _buildBox(context, context.getBoxContent(box),
                    isDragging: true),
                childWhenDragging: const SizedBox(
                  width: boxSize,
                  height: boxSize,
                ),
                child: _buildBox(context, context.getBoxContent(box)),
              )
            : BoxWidgetWithDropZone(
                box: box,
              );
      },
    );
  }

  Widget _buildBox(BuildContext context, Widget content,
      {bool isDragging = false}) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      width: boxSize,
      height: boxSize,
      decoration: BoxDecoration(
        color: Colors.greenAccent,
        borderRadius: BorderRadius.circular(16),
        boxShadow: isDragging
            ? [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 10,
                  offset: const Offset(0, 3),
                )
              ]
            : null,
      ),
      child: Center(child: content),
    );
  }
}

class BoxWidgetWithDropZone extends StatelessWidget {
  final Box box;

  const BoxWidgetWithDropZone({super.key, required this.box});

  @override
  Widget build(BuildContext context) {
    if (box.boxSet.isNotEmpty) {
      return ErrorBorderWidget(
        hasError: box.hasError,
        child: StackedBoxesWidget(boxes: [box, ...box.boxSet]),
      );
    }

    return DragTarget<Box>(
      onAccept: (value) {
        if (value.position != box.position && value.boxSet.isEmpty) {
          context.read<GameBloc>().add(RemoveBoxEvent(value, box));
        }
      },
      builder: (ctx, i1, i2) {
        final clr = i1.isNotEmpty ? Colors.tealAccent : Colors.greenAccent;
        return AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          width: boxSize,
          height: boxSize,
          decoration: BoxDecoration(
            color: clr,
            borderRadius: BorderRadius.circular(16),
          ),
          child: Center(child: context.getBoxContent(box)),
        );
      },
    );
  }
}
