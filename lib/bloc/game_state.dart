part of 'game_bloc.dart';

@immutable
abstract class GameState extends Equatable {
  @override
  List<Object?> get props => [];
}

class GameInitial extends GameState {}

class GameLoading extends GameState {}

class GameLoadError extends GameState {}

class GameLoaded extends GameState {
  final List<Box> left;
  final List<Box> right;

  GameLoaded({required this.left, required this.right});

  @override
  List<Object?> get props => [left, right];
}

class GameFinished extends GameState {
  final int errors;

  GameFinished(this.errors);

  @override
  List<Object?> get props => [errors];
}
